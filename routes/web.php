<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/', 'ShopController@index')->name('shop');
Route::get('/cart', 'ShopController@cart');
Route::post('/addToCart/{product}', 'ShopController@addToCart')->name('add_to_cart');
Route::post('/updateCart/{product}', 'ShopController@updateCart')->name('update_cart');
Route::delete('/removeFromCart/{product}', 'ShopController@removeFromCart');
Route::get('/personalData', 'ClientController@personalData')->name('personal_data');
Route::post('/buy', 'ShopController@buy')->name('buy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
