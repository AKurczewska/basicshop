<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function client()
    {
        return $this->hasOne('App\Models\Client');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'order_products');
    }

    protected $fillable = [
        'client_id',
    ];
}
