<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'surname',
        'street',
        'city',
        'postcode',
        'phone',
        'email',
        'tax_number',
    ];
}
