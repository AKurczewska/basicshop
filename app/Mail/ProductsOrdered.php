<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductsOrdered extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;
    protected $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order, $file)
    {
        $this->order = $order;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Zaproszenie do korzystania z aplikacji')
            ->markdown('email.ordered')
            ->with([
                'order' => $this->order,
            ])->attachData($this->file, 'order.pdf');
    }
}
