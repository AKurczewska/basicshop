<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddToCartRequest;
use App\Http\Requests\PersonalDataRequest;
use App\Http\Services\CartService;
use App\Models\Client;
use App\Models\Product;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class ShopController extends Controller
{
    protected $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index()
    {
        $products = Product::simplePaginate(4);

        return view('shop.list', ['products' => $products]);
    }

    public function cart()
    {
        return view('shop.cart');
    }

    public function addToCart(Product $product, AddToCartRequest $request)
    {
        $data = $request->validated();

        $this->cartService->addProduct($product, $data);

        return redirect()->back();
    }

    public function updateCart(Product $product, AddToCartRequest $request)
    {
        $data = $request->validated();
        $this->cartService->update($product, $data);
    }

    public function removeFromCart(Product $product)
    {
        $this->cartService->remove($product);
    }

    public function buy(PersonalDataRequest $request)
    {
        $personalData = $request->validated();

        if (!Auth::user())
            try {
                $client = Client::create($personalData);
            } catch(Exception $e) {
                Log::info($e->getMessage());
                Session::flash('message', Lang::get('flash.client_error'));
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('shop');
            }
        else
            $client = Client::where('user_id', Auth::id())->first();

        $this->cartService->buy($client);

        return redirect()->route('shop');
    }
}
