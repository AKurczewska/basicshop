<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function personalData()
    {
        $client = null;
        if (Auth::user())
            $client = Client::where('user_id', Auth::id())->first();

        return view('shop.personalData', compact('client'));
    }
}
