<?php

namespace App\Http\Services;

use App\Mail\ProductsOrdered;
use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use PDF;

class CartService
{
    public function addProduct(Product $product, array $data)
    {
        $cart = session()->get('cart');

        if (!$cart) {
            $cart = [
                $product->id => [
                    'name' => $product->name,
                    'amount' => $data['amount'],
                    'price' => $product->price
                ]
            ];
        } elseif (isset($cart[$product->id])) {
            $cart[$product->id]['amount'] += $data['amount'];
        } else {
            $cart[$product->id] = [
                'name' => $product->name,
                'amount' => $data['amount'],
                'price' => $product->price
            ];
        }

        if ($product->max_amount >= $cart[$product->id]['amount']) {
            session()->put('cart', $cart);
            Session::flash('message', Lang::get('flash.added_successfully'));
        } else {
            Session::flash('message', Lang::get('flash.not_enough_products'));
            Session::flash('alert-class', 'alert-danger');
        }
    }

    public function update(Product $product, array $data)
    {
        $cart = session()->get('cart');
        if (isset($cart[$product->id])) {
            if ($product->max_amount >= $data['amount']) {
                $cart[$product->id]['amount'] = $data['amount'];
                session()->put('cart', $cart);
                Session::flash('message', Lang::get('flash.cart_updated'));
            } else {
                Session::flash('message', Lang::get('flash.not_enough_products'));
                Session::flash('alert-class', 'alert-danger');
            }
        } else {
            Session::flash('message', Lang::get('flash.dont_have_product'));
            Session::flash('alert-class', 'alert-danger');
        }
    }

    public function remove(Product $product)
    {
        $cart = session()->get('cart');
        if (isset($cart[$product->id])) {
            unset($cart[$product->id]);
            session()->put('cart', $cart);
            Session::flash('message', Lang::get('flash.product_removed'));
        } else {
            Session::flash('message', Lang::get('flash.dont_have_product'));
            Session::flash('alert-class', 'alert-danger');
        }
    }

    public function buy(Client $client)
    {
        $items = session()->get('cart');
        if (!$items) {
            Session::flash('message', Lang::get('flash.empty_cart'));
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('shop');
        }

        $order = Order::create(['client_id' => $client->id]);
        foreach ($items as $productId => $item) {
            $product = Product::find($productId);
            $product->update([
                'max_amount' => ($product->max_amount - $item['amount'])
            ]);
            $order->products()->attach($productId, ['amount' => $item['amount']]);
        }
        session()->remove('cart');
        Session::flash('message', Lang::get('flash.successfully_bought'));

        $pdf = PDF::loadView('pdf.order', compact('items'));
        $pdf = $pdf->download('order.pdf');
        Mail::to($client->email)->send(new ProductsOrdered($order, $pdf));
    }
}
