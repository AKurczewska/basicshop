<?php

return [
    'client_error' => 'Błąd podczas zapisywania danych',
    'added_successfully' => 'Dodano produkt do koszyka!',
    'not_enough_products' => 'Nie ma wystarczającej liczy produktów w sklepie!',
    'cart_updated' => 'Koszyk został zaktualizowany',
    'dont_have_product' => 'Nie masz tego produktu w koszyku!',
    'product_removed' => 'Produkt został usunięty',
    'empty_cart' => 'Twój koszyk jest pusty!',
    'successfully_bought' => 'Pomyślnie zakupiono produkty',
];
