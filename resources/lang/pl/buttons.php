<?php

return [
    'shop' => 'Sklep',
    'cart' => 'Koszyk',
    'add' => 'Dodaj',
    'next' => 'Dalej',
    'login' => 'Zaloguj',
    'register' => 'Zarejestruj',
    'logout' => 'Wyloguj',
];
