<?php

return [
    'client_error' => 'Error while savig client data!',
    'added_successfully' => 'Product added to cart successfully!',
    'not_enough_products' => 'Not enough products in the shop!',
    'cart_updated' => 'Cart updated successfully',
    'dont_have_product' => 'You don\'t have this product in the cart!',
    'product_removed' => 'Product has been removed',
    'empty_cart' => 'Your cart is empty!',
    'successfully_bought' => 'Successfully bought products',
];
