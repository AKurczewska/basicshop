<?php

return [
    'shop' => 'Shop',
    'cart' => 'Cart',
    'add' => 'Add',
    'next' => 'Next',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
];
