<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <h1>{{ __('messages.shop_products') }}</h1>
        <table class="table table-hover table-condensed">
        <thead>
        <tr>
            <th>{{ __('messages.product') }}</th>
            <th>{{ __('messages.price') }}</th>
            <th>{{ __('messages.amount') }}</th>
            <th>{{ __('messages.subtotal') }}</th>
        </tr>
        </thead>
        <tbody>

            <?php $total = 0 ?>

            @foreach($items as $id => $product)
                <?php $total += $product['price'] * $product['amount'] ?>
                <tr>
                    <td><h4>{{ $product['name'] }}</h4></td>
                    <td>{{ number_format($product['price'], 2) }} zl</td>
                    <td>{{ $product['amount'] }}</td>
                    <td>{{ number_format($product['price'] * $product['amount'], 2) }}  zl</td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td colspan="2" class="hidden-xs text-center"><strong>{{ __('messages.total') }} {{ number_format($total, 2) }} zl</strong></td>
        </tr>
        </tfoot>
    </table>
    </body>
</html>