@extends('layouts.app')

@section('content')
    <div class="container">
        <div>
            <h1>{{ __('messages.shop_products') }}</h1>
        </div>
        <div>
            @foreach($products as $product)
                <div>
                    <div>
                        <h5>{{$product['name']}}</h5>
                    </div>
                    <div>
                        <p>{{number_format($product['price'], 2)}} zł</p>
                    </div>
                    <div>
                        <form action="{{route('add_to_cart', ['product' => $product->id])}}" method="post">
                            @csrf
                            <input type="number" name="amount" min="1" max="{{$product->max_amount}}" value="1" required/>
                            <input class="btn btn-info btn-sm" type="submit" value="{{ __('buttons.add') }}">
                        </form>
                    </div>
                    <br>
                </div>
            @endforeach
        </div>
        {{ $products->links() }}
    </div>
@endsection