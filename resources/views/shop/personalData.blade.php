@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ __('messages.personal_data') }}</h1>
        <form action="{{route('buy')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="name">{{ __('messages.name') }}:</label>
                <input class="form-control" id="name" name="name" @auth value="{{$client->name}}" @endauth required>
            </div>
            <div class="form-group">
                <label for="surname">{{ __('messages.surname') }}:</label>
                <input class="form-control" id="surname" name="surname" @auth value="{{$client->surname}}" @endauth required>
            </div>
            <div class="form-group">
                <label for="street">{{ __('messages.street') }}:</label>
                <input class="form-control" id="street" name="street" @auth value="{{$client->street}}" @endauth required>
            </div>
            <div class="form-group">
                <label for="city">{{ __('messages.city') }}:</label>
                <input class="form-control" id="city" name="city" @auth value="{{$client->city}}" @endauth required>
            </div>
            <div class="form-group">
                <label for="postcode">{{ __('messages.postcode') }}:</label>
                <input class="form-control" id="postcode" name="postcode" @auth value="{{$client->postcode}}" @endauth required>
            </div>
            <div class="form-group">
                <label for="phone">{{ __('messages.phone') }}:</label>
                <input class="form-control" type="tel" id="phone" name="phone" @auth value="{{$client->phone}}" @endauth
                    placeholder="+48123456789" pattern="^\+?([0-9]{2})\)?[- ]?([0-9]{9})$" required>
            </div>
            <div class="form-group">
                <label for="email">{{ __('messages.email') }}:</label>
                <input class="form-control" type="email" id="email" name="email" @auth value="{{$client->email}}" @endauth required>
            </div>
            <div class="form-group">
                <label for="tax_number">{{ __('messages.tax_number') }}:</label>
                <input class="form-control" id="tax_number" name="tax_number" @auth value="{{$client->tax_number}}" @endauth
                    pattern="^([0-9]{10})$" required>
            </div>
            <button type="submit" class="btn btn-info">{{ __('messages.buy') }}</button>
        </form>
    </div>
@endsection