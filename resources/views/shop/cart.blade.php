@extends('layouts.app')

@section('content')
    <div>
        <h1>{{ __('messages.cart') }}</h1>
    </div>
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">{{ __('messages.product') }}</th>
            <th style="width:10%">{{ __('messages.price') }}</th>
            <th style="width:8%">{{ __('messages.amount') }}</th>
            <th style="width:22%" class="text-center">{{ __('messages.subtotal') }}</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>

        <?php $total = 0 ?>

        @if(session('cart'))
            @foreach(session('cart') as $id => $product)
                <?php $total += $product['price'] * $product['amount'] ?>
                <tr>
                    <td data-th="product">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>{{ $product['name'] }}</h4>
                            </div>
                        </div>
                    </td>
                    <td data-th="price">{{ number_format($product['price'], 2) }} zł</td>
                    <td data-th="amount">
                        <input class="form-control amount" type="number" value="{{ $product['amount'] }}" min="1" />
                    </td>
                    <td data-th="subtotal" class="text-center">{{ number_format($product['price'] * $product['amount'], 2) }}  zł</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
            @endforeach
        @endif

        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"></td>
            <td colspan="2" class="hidden-xs text-center"><strong>{{ __('messages.total') }} {{ number_format($total, 2) }} zł</strong></td>
        </tr>
        </tfoot>
    </table>
    <a href="{{route('personal_data')}}" class="btn btn-success">{{ __('buttons.next') }}</a>

@endsection