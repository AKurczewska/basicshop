@component('mail::message')

{{ __('messages.order_number') }} <strong>{{$order->id}}</strong> {{ __('messages.was_made') }}.

{{ __('messages.thanks') }}<br>
{{ config('app.name') }}
@endcomponent