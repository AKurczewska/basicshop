
$(document).ready(function () {
    $(".update-cart").click(function (e) {
        e.preventDefault();

        $.ajax({
            url: 'updateCart/' + $(this).attr("data-id"),
            method: "post",
            data: { _token: $('meta[name="csrf-token"]').attr('content'), amount: $(this).parents("tr").find(".amount").val() },
            success: function (response) {
                window.location.reload();
            }
        });
    });

     $(".remove-from-cart").click(function (e) {
        e.preventDefault();

        if(confirm("Are you sure?")) {
            $.ajax({
                url: 'removeFromCart/' + $(this).attr("data-id"),
                method: "DELETE",
                data: { _token: $('meta[name="csrf-token"]').attr('content')},
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    });
});